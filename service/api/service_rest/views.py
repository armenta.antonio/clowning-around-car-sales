import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

from .encoders import (
    ServiceEncoder, 
    TechnicianEncoder,
    )

from .models import (
    AutomobileVO,
    Service, 
    Technician,
    )

fourOhfour = { 
    "response": "object does not exist" 
}

@require_http_methods(["GET", "POST"])
def service_requests(request):
    # show all active/outstanding service requests
    if request.method == "GET":
        active_jobs = Service.objects.all()
        return JsonResponse(
            {"active_requests": active_jobs},
            encoder=ServiceEncoder,
            safe=False
        )

    elif request.method == "POST":
        data = json.loads(request.body)
        try:
            c_vin=data["vin"]
            print(data)
            AutomobileVO.objects.get(vin=c_vin)
            print(AutomobileVO.objects.get(vin=c_vin))
            data["vip"] = True
        except AutomobileVO.DoesNotExist:
            data["vip"] = False
        
        service = Service.objects.create(**data)
        return JsonResponse(
          service,
          encoder=ServiceEncoder,
          safe=False
        )



@require_http_methods(["DELETE", "GET", "PUT"])
def service_request(request, pk):
    if request.method == "GET":
        try:
            service = Service.objects.get(id=pk)
            return JsonResponse(
                service,
                encoder=ServiceEncoder,
                safe=False
            )
        except Service.DoesNotExist:
            response = JsonResponse(fourOhfour)
            return response
            
    elif request.method == "DELETE":
        try:
            service = Service.objects.get(id=pk)
            service.delete()
            return JsonResponse(
                service,
                encoder=ServiceEncoder,
                safe=False,
            )
        except Service.DoesNotExist:
            return JsonResponse(fourOhfour)

    else: # PUT
        try:
            data = json.loads(request.body)
            service = Service.objects.get(id=pk)

            props = [
                "owner_name", 
                "vip", 
                "vin",
                "date_of_service",
                "time_of_service", 
                "technician", 
                "reason_for_service", 
                "completed", 
                "canceled",
                ]

            for prop in props:
                if prop in data:
                    setattr(service, prop, data[prop])
            service.save()
            return JsonResponse(
                service,
                encoder=ServiceEncoder,
                safe=False,
            )
        except Service.DoesNotExist:
            response = JsonResponse(fourOhfour)
            return response


@require_http_methods(["GET", "POST"])
def technician_requests(request):
    # show all technicians
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False
        )

    elif request.method == "POST":
        data = json.loads(request.body)

        technician = Technician.objects.create(**data)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def technician_request(request, employee_id):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(employee_number=employee_id)
            return JsonResponse(
                {"technician": technician},
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse(fourOhfour)
            return response

    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(employee_number=employee_id)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(fourOhfour)
    else: # PUT
        try:
            data = json.loads(request.body)
            name = "name"
            technician = Technician.objects.get(employee_number=employee_id)
            if name in data:
                setattr(technician, name, data[name])
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse(fourOhfour)
            return response
