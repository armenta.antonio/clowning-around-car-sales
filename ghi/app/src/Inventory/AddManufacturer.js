import React from "react";
import { Link } from "react-router-dom"

class AddManufacturer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",

        };
        this.handleNameChange= this.handleNameChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState( {name: value})

    }

    
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
     


        const locationUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
  
        if (response.ok) {  
          const newManufacturer = await response.json();


          const cleared = {
              name: '',

            };
            this.setState(cleared);
            
        } else {
            const span = document.getElementById("warning");
            const classes = span.classList;
            classes.remove("d-none");
        }
    }

    async componentDidMount() {
    
        const url = "http://localhost:8100/api/manufacturers/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({manufacture: data.manufacture});
        }
    }
    render() {
        return (
            <>
      
            <nav>
            <br></br>
            <Link to="/ListManufacturers" className="btn btn-secondary btn-sm">List of Manufacturers</Link>
            </nav>
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add A Manufacturer </h1>
                <form onSubmit={this.handleSubmit} id="add-manufacturer-form">
                <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={this.state.name}/>
                    <label htmlFor="name">Name
                    </label>
                    
                  </div>
                  
                  <div className="alert alert-danger alert-dismissible fade show  d-none" id="warning">
                    <strong>Error!</strong> The Customer ID MUST be Unique, try again.
                        <button type="button" className="btn-close" data-bs-dismiss="alert"></button>
                    </div>
    
                  <button className="btn btn-secondary btn-sm">Create</button>
                </form>
              </div>
            </div>
          </div>
          </>
        );
      }
    }
    
    export default AddManufacturer;
    