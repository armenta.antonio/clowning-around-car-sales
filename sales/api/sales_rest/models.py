
from django.db import models
from django.urls import reverse




# Create your models here.
class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200)
    vin = models.CharField(max_length=17, unique=True)

class Customer(models.Model):
    name = models.CharField(max_length=200, unique=False)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    state =models.CharField(max_length=200)
    phone = models.CharField(max_length=16)


    def get_api_url(self):
        return reverse("api_customer_list", kwargs={"pk": self.id})

    def __str__(self):
        return self.name

class Employee(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.SmallIntegerField()

    def get_api_url(self):
        return reverse("api_employee_list", kwargs={"pk": self.id})

    def __str__(self):
        return self.name

class Sale(models.Model):
    sold_veh = models.BooleanField(default=False)
    sales_price = models.PositiveIntegerField()
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.PROTECT,
    )

    employee = models.ForeignKey(
        Employee, 
        related_name= "employee",
        on_delete=models.CASCADE,
    )
    
    customer = models.ForeignKey(
        Customer, 
        related_name= "customer",
        on_delete=models.CASCADE,
    )
    def get_api_url(self):
        return reverse("api_sales_list", kwargs={"pk": self.id})


   









    

