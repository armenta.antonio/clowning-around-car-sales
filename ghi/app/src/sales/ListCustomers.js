import React from 'react';
import { Link } from "react-router-dom"


function CustomerList(props) {

  
    return (
          <>

        <h1 className="text-center"> Sales Customers {  }
        <Link to="/AddSalesCustomer" className="btn btn-secondary btn-sm">Add Customer</Link>
        </h1>
      <div className="container">
  <div className="row">
    <div className="col-12">
 

		<table className="table table-striped table-hover">
		  <thead>
		    <tr>

		      <th scope="col">Name</th>
              <th scope="col">Address</th>
              <th scope="col">City</th>
              <th scope="col">State</th>
              <th scope="col">Phone</th>
 
 		    </tr>
		  </thead>
		  <tbody>
            {props.customer.customers.map(customers => {
                return (
            <tr key={ customers.id}   >
            
		      <td>{ customers.name }</td>
              <td> { customers.address }</td>
              <td> { customers.city }</td>
              <td> {customers.state} </td>
              <td> {customers.phone }</td>
		      
		    </tr>
              );
            })}
		  </tbody>
		</table>   
    </div>
  </div>
</div>

     </>  
    )
}

export default CustomerList