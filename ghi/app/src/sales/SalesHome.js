import React from 'react';
import { Link } from "react-router-dom"
import ListOfSales from './ListOfSales';


function SalesHome(props) {
  
    return (
          <>
          <br></br>
              <div className="col-lg-6 mx-auto">
                <div className="d-grid gap-2 d-sm-flex justify-content-sm-center"> 
                  <br></br>
                  <Link to="/AddASale" className="btn btn-secondary btn-sm">Add a Sale</Link> {}
                  <Link to="/SalesDetails" className="btn btn-secondary btn-sm">Sales Per Rep Page</Link> {}
                  <Link to="/ListManufacturers" className="btn btn-secondary btn-sm" >List Of Manufacturers</Link> {  }
                  <Link to="/ListModels" className="btn btn-secondary btn-sm"  > List of Models</Link>

                 </div>
              </div>
          <nav
          style={{
            borderBottom: "solid 7px",
            paddingBottom: "1rem",
          }}
        >
        </nav>
        <h1 className="text-center"> Look Who is Selling Cars!</h1>

          <ListOfSales sales={props.sales}/>

     

     </>  
    )
}

export default SalesHome