from django.urls import path
from .views import (
    service_requests, 
    service_request, 
    technician_requests,
    technician_request,
    )

urlpatterns = [
    path(
        'service/',
        service_requests,
        name="service_requests"
    ),
    path(
        'service/<int:pk>/',
        service_request,
        name="service_request"
    ),
    path(
        'technician/',
        technician_requests,
        name="technician_requests"
    ),
    path(
        'technician/<str:employee_id>/',
        technician_request,
        name="technician_request"
    ),
]