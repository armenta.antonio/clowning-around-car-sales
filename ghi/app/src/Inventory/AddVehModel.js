import React from "react";
import { Link } from "react-router-dom"

class AddVehModelForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name:"",
            picture_url:"",
            manufacturer_id:"",
            manufacturers:[],

        };
        this.handleNameChange= this.handleNameChange.bind(this);
        this.handlePictureUrlChange= this.handlePictureUrlChange.bind(this);
        this.handleManufacturerChange= this.handleManufacturerChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState( {name: value})
    }
    handlePictureUrlChange(event) {
        const value = event.target.value;
        this.setState( { picture_url: value})
    }

    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState( {manufacturer_id: value})
    }

    
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
      delete data.manufacturers;


        const locationUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
          },
        };

        const response = await fetch(locationUrl, fetchConfig);
      
  
        if (response.ok) {  
          const newVehModel = await response.json();


          const cleared = {
            name: "",
            picture_url: "",
            manufacturer_id:"",

            };
            this.setState(cleared);
            
        } else {
            const span = document.getElementById("warning");
            const classes = span.classList;
            classes.remove("d-none");
        }
    }

    async componentDidMount() {
    
        const url = "http://localhost:8100/api/manufacturers/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({manufacturers: data.manufacturers});
        }
    }
    render() {
        return (
            <>
      
            <nav>
            <br></br>
            <Link to="/ListModels" className="btn btn-secondary btn-sm">List of Models</Link>
            </nav>
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add A Vehicle Model </h1>
                <form onSubmit={this.handleSubmit} id="add-model-form">
                <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} placeholder="name" required type="text" name="name" id="name" className="form-control" value={this.state.name}/>
                    <label htmlFor="name">Name</label>
                    
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handlePictureUrlChange} placeholder="picture_url" required type="url" name="picture_url" id="picture_url" className="form-control" value={this.state.picture_url}/>
                    <label htmlFor="picture_url">Picture URL</label>
                    <small className="text-secondary">Format: https://example.com</small>
                    
                  </div>
                  <div className="mb-3">
              <select onChange={this.handleManufacturerChange} value={this.state.manufacturer_id} required name="manufacturer" id="manufacturer" className="form-select"  > 
                    <option value="">Choose Vehicle Manufacturer
                    </option>
                    {this.state.manufacturers.map(manufacturer => {
                        return ( 
                        <option key={manufacturer.id} value={manufacturer.id}>
                            {manufacturer.name}
                        </option>
                        );
                    })}
                    </select>
                    <small className="text-secondary">If Manufacturer Is Not Listed, Please:   
                    <Link to="/AddManufacturer" type="button" className="btn btn-link">Add A Vehicle Manufacturer</Link>
                    </small>
              </div>
             
                  
                  <div className="alert alert-danger alert-dismissible fade show  d-none" id="warning">
                    <strong>Error!</strong> The Please Try Again
                        <button type="button" className="btn-close" data-bs-dismiss="alert"></button>
                    </div>
    
                  <button className="btn btn-secondary btn-sm">Create</button>
                   
                </form>
              </div>
            </div>
          </div>
          </>
        );
      }
    }
    
    export default AddVehModelForm;
    