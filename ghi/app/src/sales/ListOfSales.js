import React from 'react';



// props will have a value = sales array

function ListOfSales(props) {
  
    return (
          <>
	
      <div className="container">
        <div className="row">
            <div className="col-12">
 

		<table className="table table-striped">
		  <thead>
		    <tr>

		      <th scope="col">Sales Rep</th>
		      <th scope="col">Customer</th>
		      <th scope="col">VIN</th>
		      <th scope="col">Sales Price</th>
 
		    </tr>
		  </thead>
		  <tbody>
            {props.sales.map(sale => {
                return (
            <tr key={ sale.automobile.vin }   >

		      <td>{ sale.employee.name }</td>
		      <td>{ sale.customer.name }</td>
		      <td>{ sale.automobile.vin }</td>
		      <td> ${ sale.sales_price }</td>
		    </tr>
              );
            })}
		  </tbody>
		</table>   
    </div>
  </div>
</div>

     </>  
    )
}

export default ListOfSales 