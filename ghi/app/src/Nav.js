import { NavLink } from 'react-router-dom';
import ServiceList from './service/ListServiceRequests';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-success bg-gradient bg-opacity-25">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Clown Cars Auto Sales and Service</NavLink>

        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" 
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" 
                aria-expanded="false" aria-label="Toggle navigation">
                  
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li>
              <NavLink className="nav-link" to="/SalesHome">Sales</NavLink>
              </li>
            <li>
              <NavLink onClick={ServiceList.fetchData} className="nav-link" to="/service">Service</NavLink>
              </li>
          <li>
              <NavLink className="nav-link" to="/ListAutoInventory">Inventory</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/ListCustomers">Customers</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
