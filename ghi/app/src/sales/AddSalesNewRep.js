import React from "react";
import { Link } from "react-router-dom"

class AddSalesNewRep extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            employee_number: "",

        };
        this.handleNameChange= this.handleNameChange.bind(this);
        this.handleEmployeeNumberChange= this.handleEmployeeNumberChange.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState( {name: value})
    }

    handleEmployeeNumberChange(event) {
        const value = event.target.value;
        this.setState( {employee_number: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};


        const locationUrl = 'http://localhost:8090/api/employees/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
  
        if (response.ok) {  
          const newEmployee = await response.json();

          const cleared = {
              name: '',
              employee_number: '',
            };
            this.setState(cleared);
            
        } else {
            const span = document.getElementById("warning");
            const classes = span.classList;
            classes.remove("d-none");
        }
    }

    async componentDidMount() {
    
        const url = "http://localhost:8090/api/employees/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({employee: data.employee});
        }
    }
    render() {
        return (
            <>
      
            <nav>
            <br></br>
            <Link to="/SalesHome" className="btn btn-secondary btn-sm">Back To Sales Home</Link>
            </nav>
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add a Sales Rep </h1>
                <form onSubmit={this.handleSubmit} id="add-employee-form">
                <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={this.state.name}/>
                    <label htmlFor="name">Name
                    </label>
                    
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleEmployeeNumberChange} placeholder="employeeNumber" required type="number" name="employeeNumber" id="fabric" className="form-control" value={this.state.employee_number}/>
                    <label htmlFor="employeeNumber">Employee Number</label>
                    <small className="text-secondary"> Must Be 4 Digits -No More No Less-</small>
                    
                  </div>

                  <div className="alert alert-danger alert-dismissible fade show  d-none" id="warning">
                    <strong>Error!</strong> The Please Try Again.
                        <button type="button" className="btn-close" data-bs-dismiss="alert"></button>
                    </div>
    
                  <button className="btn btn-secondary btn-sm">Create</button>
                </form>
              </div>
            </div>
          </div>
          </>
        );
      }
    }
    
    export default AddSalesNewRep;
    