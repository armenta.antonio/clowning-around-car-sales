from django.urls import path

from .views import (
    api_sales_list, 
    api_show_sales, 
    api_show_customers,
    api_customer_list,
    api_employee_list,
    api_show_employees,
)





urlpatterns = [
    path("sales/", api_sales_list, name="api_list_sales"),
    path("sales/<int:pk>/",api_show_sales, name="api_show_sales"),
    
    path("customers/", api_customer_list, name="api_customers"),
    path("customers/<int:pk>/", api_show_customers, name="api_show_customers"),

    path("employees/", api_employee_list, name="api_employee"),
    path("employees/<int:pk>/", api_show_employees, name="api_show_employees"),
   
]