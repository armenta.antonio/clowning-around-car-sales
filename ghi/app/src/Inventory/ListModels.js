import React from 'react';
import { Link } from "react-router-dom"


function ModelsList(props) {

    return (
        <>
   
        <h1 className="text-center"> All Vehicle Models
        <Link to="/AddVehModel" className="btn btn-secondary btn-sm">Add A Vehicle Model  { } </Link>
        </h1>
      <div className="container">
  <div className="row">
    <div className="col-12">
 

		<table className="table table-striped table-hover">
		  <thead>
		    <tr>

		      <th scope="col">Name</th>
              <th scope="col">Manufacturer</th>
              <th scope="col">Picture</th>
 		    </tr>
		  </thead>
		  <tbody>
            {props.model.models.map(model => {
                return (
            <tr key={ model.id}   >
            
		      <td>{ model.name }</td>
              <td> {model.manufacturer.name}</td>
		      <td className="w-25">
			      <img src={model.picture_url}  className="img-fluid img-thumbnail" alt=""/>
                  </td>
		    </tr>
              );
            })}
		  </tbody>
		</table>   
    </div>
  </div>
</div>

     </>  
    )
}

export default ModelsList