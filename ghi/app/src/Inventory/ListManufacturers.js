import React from 'react';
import { Link } from "react-router-dom"


function ManufacturerList(props) {

  
    return (
          <>
         
        <h1 className="text-center"> List of Manufacturers <Link to="/AddManufacturer" className="btn btn-secondary btn-sm">Add New Manufacturer</Link></h1>
        
      <div className="container">
  <div className="row">
    <div className="col-12">
 

		<table className="table table-striped">
		  <thead>
		    <tr>

		      <th scope="col">Name</th>
 		    </tr>
		  </thead>
		  <tbody>
            {props.manufacturer.manufacturers.map(manufacturers => {
                return (
            <tr key={ manufacturers.name}   >

		      <td>{ manufacturers.name }</td>

		    </tr>
              );
            })}
		  </tbody>
		</table>   
    </div>
  </div>
</div>

     </>  
    )
}

export default ManufacturerList