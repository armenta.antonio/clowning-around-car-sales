import React from "react";
import '../index.css';
import { Link, withRouter } from 'react-router-dom'
import ServiceNav from "./ServiceNav";
import EditServiceForm from "./EditServiceForm";

class ServiceList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      active_requests: [],
      filter: "",
    };
    this.handleFilter = this.handleFilter.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleComplete = this.handleComplete.bind(this);
  }
  async componentDidMount() {
    this.fetchData()
  }
  async fetchData() {
    fetch('http://localhost:8080/api/service/')
      .then(response => response.json())
      .then(data => this.setState({ active_requests: data.active_requests }));
  }

  // CANCEL HANDLER
  async handleCancel(id, bool) {
    if (bool === "cancel") {
      bool = true
    } else bool = false;
    const url = `http://localhost:8080/api/service/${id}/`;
    const fetchConfig = {
      method: "PUT",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ canceled: bool })
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      this.fetchData()
    }
  }

  // COMPLETE HANDLER
  async handleComplete(id) {
    const updateURl = `http://localhost:8080/api/service/${id}/`;
    const fetchConfig = {
      method: "PUT",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ completed: true })
    };
    const response = await fetch(updateURl, fetchConfig);
    if (response.ok) {
      this.fetchData()
    }
  }

  // SEARCH FILTER HANDLER
  async handleFilter(event) {
    const value = event.target.value;
    this.setState({ filter: value })
  }


  render() {
    return (
      <>
        <ServiceNav />

        {/* SEARCH BAR */}
        <div className="input-group mb-3 ">
          <input onChange={this.handleFilter} type="text" 
                 className="form-control" 
                 placeholder="enter vin to filter list" />
        </div>

        {/* <div className="form-check">
          <input className="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
            <label className="form-check-label" for="flexCheckDefault">
              Show/hide cancelled
              </label>
              </div> */}

        <table className='table text-center align-middle table-borderless table-hover'>

          <thead>
            <tr>
            <th></th>
              <th>vin</th>
              <th>owner</th>
              <th>date</th>
              <th>time</th>
              <th>technician</th>
              <th>reason</th>
            </tr>
          </thead>

          <tbody>

            {this.state.active_requests.map((service) => {

              // DEFAULT TABLE FORMATTING VALUES
              let completedCancel = "btn btn-outline-danger btn-space"
              let completedSuccess = "btn btn-outline-success btn-space"
              let complete = "complete"
              let cancel = "cancel"
              let highlightRow = ""
              let hideRowToggle = ""

              // CONVERT TIME FORMAT TO NOT UGLY
              function formattedTime(ftime) {
                ftime.slice()
                if (Number(ftime.charAt(0)) === 0) {
                  return ftime.substr(1, 4)
                }
                else return ftime.substr(0, 5)
              }

              // FORMAT VIP LINE ITEM
              if (service.vip) {
                highlightRow = "table-info"
              }

              // FORMAT COMPLETED LINE ITEM
              if (service.completed) {
                completedCancel = "d-none"
                completedSuccess = "btn btn-success disabled"
                complete = "completed"
              }

              // FORMAT CANCELED LINE ITEM
              if (service.canceled) {
                completedSuccess = "d-none"
                cancel = "uncancel"
                hideRowToggle = "d-none"
              }

              // SEARCH BAR FILTER LOGIC
              if (this.state.filter !== "") {
                if (service.vin === this.state.filter) {
                  hideRowToggle = ""
                }
                else {
                  hideRowToggle = "d-none"
                }
              }

              return (
                
                <tr className={hideRowToggle} key={service.id}>                    
                  <Link to="/service/EditServiceForm" state={{ id: service.id }}>
                    <button className="btn btn-outline-dark">☰</button>
                  </Link>                    
                  <td className={highlightRow}>{service.vin}</td>
                  <td className={highlightRow}>{service.owner_name}</td>
                  <td className={highlightRow}>{service.date_of_service}</td>
                  <td className={highlightRow}>{formattedTime(service.time_of_service)}</td>
                  <td className={highlightRow}>{service.technician.name}</td>
                  <td className={highlightRow}>{service.reason_for_service}</td>

                  <td> {/* COMPLETE / CANCEL BUTTONS */}
                    <button className={completedSuccess} onClick={() => this.handleComplete(service.id)} 
                            to="">{complete}</button>
                    <button className={completedCancel} onClick={() => this.handleCancel(service.id, cancel)} 
                            to="">{cancel}</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <p>*VIP customers are noted above in blue</p>
      </>
    )
  }
}

export default ServiceList;