import React from 'react';
import { Link } from "react-router-dom"


function InventoryList(props) {
  
    return (
          <>

        <h1 className="text-center"> Current Vehicle Inventory {  }
        <Link to="/AddInventory" className="btn btn-secondary btn-sm">Add To Inventory</Link>
        </h1>
      <div className="container">
  <div className="row">
    <div className="col-12">
 

		<table className="table table-striped table-hover">
		  <thead>
		    <tr>

		      <th scope="col">VIN</th>
              <th scope="col">Color</th>
              <th scope="col">Year</th>
              <th scope="col">Model</th>
              <th scope="col">Manufacturer</th>
              <th scope='col'>Picture</th>
 		    </tr>
		  </thead>
		  <tbody>
            {props.inventory.autos.map(auto => {
                return (
            <tr key={ auto.id}   >
            
		      <td>{ auto.vin }</td>
              <td> { auto.color }</td>
              <td> { auto.year }</td>
              <td> {auto.model.name} </td>
              <td> {auto.model.manufacturer.name }</td>
		      <td className="w-25">
			      <img src={auto.model.picture_url}  className="img-fluid img-thumbnail" alt=""/>
                  </td>
		    </tr>
              );
            })}
		  </tbody>
		</table>   
    </div>
  </div>
</div>

     </>  
    )
}

export default InventoryList