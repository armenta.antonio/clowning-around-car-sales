import React from 'react';
import { Link } from "react-router-dom"

class SalesDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sales: props.sales,
            employee: undefined,
            employees: [],
            employee_sales:[],
        };
        this.handleEmployeeChange = this.handleEmployeeChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleEmployeeChange(event){
        const tmp = [];
        const employee_id = event.target.value;
        for (const item of this.state.sales) {

            if (item.employee.id === parseInt(employee_id)) {
                tmp.push(item);
            }
        }
        this.setState( {employee_sales: tmp });
        
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        const submitDict = {
            employee: data.employee,
        }
        const locationUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(submitDict),
          headers: {
              'Content-Type': 'application/json',
          },
        };

    }

    async getEmployee(){
        const url = "http://localhost:8090/api/employees/";
        const response = await fetch(url);


        if (response.ok) {
            const data = await response.json();
            this.setState({employees: data.employees });
        } 
    }
    async getSales(){
        const url = "http://localhost:8090/api/sales/";

        const response = await fetch(url);


        if (response.ok) {
            const data = await response.json();
            this.setState({sales: data.sales });
        }
    }
    async getCustomer(){
        const url = "http://localhost:8090/api/customers/";

        const response = await fetch(url);


        if (response.ok) {
            const data = await response.json();
            this.setState({customers: data.customers });
        }
    }
    async getAutomobile(){
        const url = "http://localhost:8100/api/automobiles/";

        const response = await fetch(url);


        if (response.ok) {
            const data = await response.json();
            this.setState({autos: data.autos });
        }
    }
    async componentDidMount() {
        this.getEmployee()
        this.getSales()
        this.getCustomer()
        this.getAutomobile()
 
    }
    
    render() {
        return (
            <>
            <nav>
                <br></br>
            <Link to="/SalesHome" className="btn btn-secondary btn-sm">Back To Sales Home</Link>
            </nav>
                <h1>Search for a Sales Rep </h1>

                    
                <div className="mb-3">
                    <select onChange={this.handleEmployeeChange} value={this.state.employee} required name="employee" id="employee" className="form-select"  > 
                    <option value="">Choose Sales Rep</option>
                    {this.state.employees.map(employee => {
                        return (
                        <option key={employee.id} value={employee.id}>
                            {employee.name} 
                        </option>
                        );
                    })}
                    </select>
    
                </div>
                <div className="container">
                <div className="row">
                    <div className="col-12">
                        <table className="table table-striped">
                        <thead>
                            <tr>
                    
                            <th scope="col">Sales Rep</th>
                            <th scope="col">Customer</th>
                            <th scope="col">VIN</th>
                            <th scope="col">Sales Price</th>
                
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.employee_sales.map(sale => {
                                return (
                                <tr key={sale.id + sale.employee.name} >
                                <td>{sale.employee.name}</td>
                                <td>{ sale.customer.name} </td>
                                <td> {sale.automobile.vin}</td>
                                <td> {sale.sales_price}</td>
                                


                                </tr>
                                );
                            })}
                        </tbody>
                        </table>   
                    </div>
                </div>
                </div>


            </>
        )
    }

}

export default SalesDetails


 