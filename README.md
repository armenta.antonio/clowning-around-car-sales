# Clown Car Auto Sales and Service

Team:

* Stephen "Matty" Mccain  Sales
* antonio - service

## Design
We began with outlining the major implementations needed for each microservice and then how they would connect with the inventory.
Each ms is accessible from the nav and each have their own nav for navigation within their respective ms. the intention here was to keep the main nav
    from feeling cluttered and to provide contextual navigation of the site.

## Service microservice

the service ms handles the creation of appointment(Service) objects and Technician objects.
the Technician model is very simple, but I elected to use the employee_id as the PK, using the 'primary_key' argument, in lieu of the built-in ID which 
    should ideally make it more usable. as a result, the employee_id property must be unique.
the Service model stores data about the individual car, owner, and appointment details as well as hidden properties to indicate whether a service has 
    or has not been completed. the integration point is on object creation, and checks the provided VIN against the inventory to determine if the 
    car/owner is 'VIP' which is then stored on the object. I had considered putting that check elsewhere but decided it ultimately wouldn't matter 
    because a car shouldn't be added to the the available/sold inventories after a service request had been created.
the AutomobileVO model is where the VIN is pulled in from inventory. its pretty thrilling.


## Sales microservice

Models: I chose to make Customer, Sales and Employees all different models as bounded context
As well as an AutomobileVO for polling to grab info from the inventory to obtain what is in stock
-why? there might be an instance when you would want to do something different with employee or customers later and not need sale info
such as adding service customers from sale customers, or to send marketing to sales or service customers
the integration with the inventory, we could see all cars purchased by a certain customer or if a veh come into inventory 
for sale again. The foreign keys played a large roll in accessing information as needed
-from the apps.js stand point I tried to make the information accessible from many different stand point, providing props for almost each page
-from a Ui experience I tried to put myself in the shoes of an employee and how I would navigate the page, proper placement of links to make the flow of
the pages work from that stance. 
**there would likely be an issue if a vehicle comes back around for a second sale, as we are currently set up to not allow a 
car to be sold more than once
customers that are listed with the sales departments could be marketed to for service on there own, with no contact with employee or sales models 
references
