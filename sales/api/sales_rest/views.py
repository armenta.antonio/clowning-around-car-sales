from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.shortcuts import render
import json
from .models import Sale, Customer, Employee, AutomobileVO
from common.json import ModelEncoder
from django.core.exceptions import ObjectDoesNotExist

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties =[ 
        "import_href",
        "vin",
    ]
class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "city",
        "state",
        "phone",
        "id",
    ]
class EmployeeEncoder(ModelEncoder):
    model = Employee
    properties = [ 
        "name",
        "employee_number",
        "id",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "employee", 
        "sold_veh",
        "sales_price",  
        "customer",
        "automobile",
        
    ]
    encoders = {
        "customer": CustomerEncoder(),
        "employee": EmployeeEncoder(),
        "automobile": AutomobileVOEncoder(),
    }





@require_http_methods(["GET", "POST"])
def api_sales_list(request):
    if request.method == "GET":
      sales = Sale.objects.all()
      return JsonResponse(
        {"sales":sales},
        encoder=SaleEncoder,
        safe=False
      )
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=400,
            )
        try:
            employee = Employee.objects.get(id=content["employee"])
            content["employee"] = employee
        except Employee.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee id"},
                status=400,
            )
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
        except Employee.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile vin"},
                status=400,
            )

        sale = Sale.objects.create(**content)
        
        return JsonResponse(
          sale,
          encoder=SaleEncoder,
          safe=False
        )

@require_http_methods(["DELETE", "GET", "PUT"])       
def api_show_sales(request, pk):
    if request.method == "GET":
        sales = Sale.objects.get(id=pk)
        return JsonResponse(
            sales, 
            encoder=SaleEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Sale.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        try:
            if "sale" in content:
                sale = Sale.objects.get(sale=content["sale"])
                content["sale"] = sale
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid "},
                status=400,
            )
        Sale.objects.filter(id=pk).update(**content)

        sale = Sale.objects.get(id=pk)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )
    
@require_http_methods(["GET", "POST"])
def api_customer_list(request):
    if request.method == "GET":
      customers = Customer.objects.all()
      return JsonResponse(
        {"customers": customers},
        encoder=CustomerEncoder,
        safe=False
      )
    else:
        content = json.loads(request.body)
        customers = Customer.objects.create(**content)
        return JsonResponse(
          customers,
          encoder=CustomerEncoder,
          safe=False
        )
@require_http_methods(["DELETE", "GET", "PUT"]) 
def api_show_customers(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer, 
            encoder=CustomerEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        try:
            if "customer" in content:
                customer = Customer.objects.get(customer=content["customer"])
                content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid "},
                status=400,
            )
        Customer.objects.filter(id=pk).update(**content)

        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
@require_http_methods(["GET", "POST"])  
def api_employee_list(request):
    if request.method == "GET":
      employees = Employee.objects.all()
      return JsonResponse(
        {"employees": employees},
        encoder=EmployeeEncoder,
        safe=False
      )
    else:
        content = json.loads(request.body)
        employees = Employee.objects.create(**content)
        return JsonResponse(
          employees,
          encoder=EmployeeEncoder,
          safe=False
        )

@require_http_methods(["DELETE", "GET", "PUT"]) 
def api_show_employees(request, pk):
    if request.method == "GET":
        employee = Employee.objects.get(id=pk)
        sales = employee.employee.all()
        return JsonResponse(
            sales, 
            encoder=SaleEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Employee.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        try:
            if "employee" in content:
                employee = Employee.objects.get(employee=content["employee"])
                content["employee"] = employee
        except Employee.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid "},
                status=400,
            )
        Employee.objects.filter(id=pk).update(**content)

        employee = Employee.objects.get(id=pk)
        return JsonResponse(
            employee,
            encoder=EmployeeEncoder,
            safe=False,
        )